<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
      $user_email   = Auth::user()->email;
        $user   =Auth::user();
        if($user->hasRole('admin')) {

            return 'admin';

        }
        elseif ($user->hasRole('normal')){

            return 'normal';

        }
        elseif ($user->hasRole('manager')){

            return $user_email;
        }
        elseif ($user->hasRole('owner')){
            return "owner";
        }
        elseif ($user->hasRole('sales')){
            return "sales";
        }
        elseif ($user->hasRole('client')){
            return "client";
        }
        else
        {
            return 'no role';
        }

    }
}
