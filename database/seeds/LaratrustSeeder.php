<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();

        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        $Adminrole = \App\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin'
        ]);

        $Managerrole = \App\Role::create([
            'name' => 'manager',
            'display_name' => 'Manager',
            'description' => 'Manager'
        ]);

        $Userrole = \App\Role::create([
            'name' => 'user',
            'display_name' => 'Normal',
            'description' => 'Normal'
        ]);
        $Ownerrole = \App\Role::create([
            'name' => 'owner',
            'display_name' => 'Owner',
            'description' => 'Owner'
        ]);
        $Clientrole = \App\Role::create([
            'name' => 'client',
            'display_name' => 'Client',
            'description' => 'Client'
        ]);
        $Salesrole = \App\Role::create([
            'name' => 'sales',
            'display_name' => 'Sales',
            'description' => 'Sales'
        ]);


        $user = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@crm.com',
            'password' => bcrypt('password')
        ]);

        $user->attachRole($Adminrole);


        $userManager = \App\User::create([
            'name' => 'admin',
            'email' => 'manager@crm.com',
            'password' => bcrypt('password')
        ]);
        $userManager->attachRole($Managerrole);

        $userNormal = \App\User::create([
            'name' => 'admin',
            'email' => 'user@crm.com',
            'password' => bcrypt('password')
        ]);
        $userNormal->attachRole($Userrole);


        $Owner = \App\User::create([
            'name' => 'owner',
            'email' => 'owner@crm.com',
            'password' => bcrypt('password')
        ]);
        $Owner->attachRole($Ownerrole);


        $Sales = \App\User::create([
            'name' => 'sales',
            'email' => 'sales@crm.com',
            'password' => bcrypt('password')
        ]);
        $Sales->attachRole($Salesrole);

        $Client = \App\User::create([
            'name' => 'client',
            'email' => 'client@crm.com',
            'password' => bcrypt('password')
        ]);
        $Client->attachRole($Clientrole);

    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\User::truncate();
        \App\Role::truncate();
        \App\Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
